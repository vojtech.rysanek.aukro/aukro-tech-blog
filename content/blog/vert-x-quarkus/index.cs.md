---
title: Vert.x a Quarkus.io
date: 2021-03-21T23:00:00.000+00:00
publishdate: 2021-03-22T09:00:00.000+00:00
tags:
- concurrency
- vert.x
- java
- stack
comments: false
hide_cover_on_detail: false
---

Přestože náš hlavní stack je Spring, neustále vyhodnocujeme nové technologie a postupy. Ty pak zavádíme do našich služeb a dosahujeme tak výrazných zlepšení v daných oblastech jejich využití.

Pro nově vznikající mikroslužby (microservices) využíváme Vert.x, u větších celků využíváme frameworku Quarkus. Výhodou obou je jejich lightweight forma a vysoký výkon, kterého se díky event-based architektuře dá dosahovat. Mikroslužby jsou řešeny technologií Kubernetes-first, čímž se jak uvidíme dále dosahuje velmi dobrých výsledků.

Díky internímu event-busu (event-based architektuře) dokáže Vert.x a Quarkus velice efektivně využívat zdroje, a to včetně spojení na databázi. Startuje velice rychle, paměťově nenáročně, což je dokonalý setup pro použití na platformě Kubernetes a zejména tam, kde vyžadujeme vysoký výkon zpracování.

![quarkus a jeho výkon](quarkus-metrics.png)

Vyšší úrovně zpracování lze dosáhnout zkompilováním aplikace do nativního kódu - [https://www.graalvm.org/](https://www.graalvm.org/) čímž lze dosáhnout další level rychlosti a nezanedbatelné paměťové úspornosti.

## Výkon {#výkon}

Pro představu následující srovnání, viz [https://www.techempower.com/benchmarks/](https://www.techempower.com/benchmarks/) - Vert.x se umisťuje aktuálně (2021) na 28. pozici se score 340k.

![Benchmark 1](fortunes1.png "benchmark vert.x")

* Spring na 317. místě se score 23k
* Quarkus (framework s backendem vert.x) na 39. místě se score 298k.
* V kompozitním score je vert.x / quarkus na 13/15 místě

![Composite](composite.png "benchmark vert.x")

Spring obsadil 51. místo

![Spring](spring.png "benchmark vert.x")

Uvedené výsledky potvrzují několikanásobné zvýšení jeho výkonu.

## Použití {#použití}

V našich aplikacích aktuálně provozujeme na platformě Vert.x microservice na zpracovávání vzniklých události, a nabízíme tak možnost upozornění klientů přes webhook. Na Gitlab CI jsme pro uvedenou službu zřídili Continuous Delivery pipeline, která umožňuje službu snadno a rychle iterovat a kontinuálně změny zavádět do produkce.

## Zdroje:

* [https://www.manning.com/books/vertx-in-action](https://www.manning.com/books/vertx-in-action "https://www.manning.com/books/vertx-in-action")
  * Zdrojový kód [https://www.manning.com/downloads/1874](https://www.manning.com/downloads/1874 "https://www.manning.com/downloads/1874")
    * Multilayer services
      * With health, readiness
      * Skaffold
* [https://developers.redhat.com/ebooks](https://developers.redhat.com/ebooks "https://developers.redhat.com/ebooks")

Github repa

* [https://github.com/vert-x3/vertx-examples](https://github.com/vert-x3/vertx-examples "https://github.com/vert-x3/vertx-examples")

Quarkus

* Framework
* [https://quarkus.io/](https://quarkus.io/ "https://quarkus.io/")
* Optimalizavane na instantni start, kubernetes
* Repa
  * [https://resources.oreilly.com/examples/0636920353171](https://resources.oreilly.com/examples/0636920353171 "https://resources.oreilly.com/examples/0636920353171")
  * [https://github.com/Apress/beginning-quarkus-framework](https://github.com/Apress/beginning-quarkus-framework "https://github.com/Apress/beginning-quarkus-framework")

{{< jobs >}}