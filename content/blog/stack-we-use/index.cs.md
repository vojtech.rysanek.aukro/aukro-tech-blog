---
title: "Náš Stack"
date: 2020-01-01T11:40:11+02:00
publishdate: 2020-01-01T11:40:11+02:00
tags: 
- stack
comments: false
hide_cover_on_detail: true
---
Základem našich služeb je Java se Spring MVC & Spring Boot, spolu s Angularem na frontendu a PostgreSQL s Elastic Search jako hlavní databází. Použiváme RabbitMQ jako service bus pro asynchronní komunikace mezi službami. 

Pro doplňkové služby využíváme i .NET Core, Microsoft SQL, Python a další.

Náš stack:

![Our stack](stack-share.png)

{{< jobs >}}