---
title: "Stack we use"
date: 2020-01-01T11:40:11+02:00
publishdate: 2020-01-01T11:40:11+02:00
tags: 
- stack
comments: false
hide_cover_on_detail: true
---
Aukro's stack includes Java with Spring MVC & Spring Boot on the backend, Angular on the frontend and PostgreSQL as main database. Elasticsearch and Redis is powering search and caching features, RabbitMQ is used as the service bus for async communication between services.

You can find details of out stack here:

![Our stack](stack-share.png)