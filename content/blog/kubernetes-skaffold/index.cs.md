---
title: "Kubernetes a Skaffold"
date: 2021-01-15T11:40:11+02:00
publishdate: 2021-01-15T11:40:11+02:00
tags:
- kubernetes
- docker
- skaffold
comments: false
hide_cover_on_detail: true
---

## Proč Kubernetes (k8s)? {#proč}

Původní architektura a infrastruktura při přechodu z polského Allegra do českých rukou byla navržena poměrně složitě a těžkopádně. Stálě je nám bližší pozice startupu než těžkopádného korporátu, máme vyšší ambice, např. umět infrastrukturu popsat kódem, včetně jednotlivých kroků deploymentu pro různá prostředí.

Před přechodem na Kubernetes jsme nasazovali aplikace pomocí Ansible. Správa, firewally a další různá nastavení však probíhala manuálně. Zřízení nového vývojového prostředí byla netriviální časově nákladná činnost, cílem byla maximální automatizace s využitím řady dalších výhod, kterých s dockerizací a Kubernetizací dosahujeme, což je patrné i z dalšího popisu.

Jedním z našich cílů je možnost provozovat celé prostředí v cloudu bez závislosti na HW a polo-automatizovaném setupu pravidel firewallu a dalších komponent infrastruktury.

## Tooling {#tooling}

### KubeCtl

Základí CLI, které technologie Kubernetes poskytuje.

### K9s

[https://k9scli.io/](https://k9scli.io/)

Výborný consolový lightweight manažer, ideálně se hodící pro přehled nad vývojovým namespacem - <a name="skaffold">Skaffoldem</a>.

### Micro8s - Lens

[https://k8slens.dev/](https://k8slens.dev/)

Dashboard, monitorující dění na clusteru děje, v tomto případě formou "tlustého" klienta s GUI.

### Kube-ps1

[https://code.kiwi.com/kubernetes-goodies-for-your-local-workspace-c17222ab57ed](https://code.kiwi.com/kubernetes-goodies-for-your-local-workspace-c17222ab57ed)

Zkratky pro powershell pro Windows vývojáře.

# Skaffold {#skaffold}

{{<blockquote>}}Cloud development on steroids {{</blockquote>}}
S plně dockerizovanou aplikací přenesenou do platformy Kubernetes se nám plně otevírají nové rozměry dalšího vývoje. Celé prostředí lze spustit v cloudu a vhodným naroutování portů aplikace funguje z uživatelského pohledu identicky jakoby běžela lokálně na vývojářském PC. Pro upřesnění - tuto možnost používáme jako náhradu a doplnění k [vývoji v cloudu]({{< ref "/blog/cloud-development" >}} "vývoji v cloudu").

Popsaným způsobem výrazně odlěhčíme práci prostředkům, následně dosahujeme snížení nároků na vývoj, což je nezanedbatelnž efekt celého procesu. Např. spuštěním celé platformy bychom na vývojářském PC vyčerpali cca 15 GB RAM, které ale využitím skaffoldu ušetříme, takže na tomto lokálním PC prakticky běží pouze IDE. Intellij IDEA má pro tento setup plně funkční integraci včetně debugu, Visual Studio Code je schopen fungovat podobně.

Pro případ, že není k dispozici internet, lze kompletně pracovat na lokálním PC a celý setup nasazovat do minikube.

## Další zdroje

[https://skaffold.dev/docs/references/cli/#skaffold-run](https://skaffold.dev/docs/references/cli/#skaffold-run)

[https://cloud.google.com/code/docs/intellij/deploying-a-k8-app](https://cloud.google.com/code/docs/intellij/deploying-a-k8-app)

[https://cloud.google.com/code/docs/intellij/quickstart-k8s](https://cloud.google.com/code/docs/intellij/quickstart-k8s)

[https://github.com/GoogleContainerTools/skaffold](https://github.com/GoogleContainerTools/skaffold)

[https://github.com/devspace-cloud/devspace](https://github.com/devspace-cloud/devspace)

{{< jobs >}}