---
title: "Vývoj v Aukru"
date: 2020-03-01T11:40:11+02:00
publishdate: 2020-03-01T11:40:11+02:00
tags: ["process"]
comments: false
hide_cover_on_detail: true
---

Rozvíjíme a udržujeme platformu, která má svoje výzvy - pro představu pracujeme s následujícími čísly (září 2021)

* 98 miliónů inzerátů
  * Měsíční nárůst inzerce cca 2 milióny nových inzerátů
* Na zveřejněné nabídky na naší platformě evidujeme 3.8 miliónů příhozů měsíčně, což je cca 128 tisíc denně
  * V peaku máme 100 příhozů/s
* PostgreSQL DB ~ velikost 1.4 TB
  * Pracujeme na partitioningu a lepším doménovém rozdělení
* Elastic Search - 200 GB, 8 shard
* 14 TB obrázků
* 200 GB faktur

Pracujeme v distribuovaných teamech napříč celou ČR/zahraničím. K tomuto účelu používáme vyladěný stack nástrojů, který nám umožňuje v takovém prostředí fungovat co nejefektivněji. Díky tomuto modelu nás COVID-19 v tomto ohledu nijak neomezil, homeoffice fungoval i tak výborně, přestože osobní kontakt má svoje výhody.

## SCRUM {#scrum}

Pro běh projektu využíváme agilní metodologii scrumu, kterou jsme upravili pro naše potřeby, čímž jsme dosáhli vyšší efektivnosti v teamové práci.

* 2 týdenní sprint
* Releasujeme nezávislé na sprintu (netřeba čekat, na jeho dokončení)
* Ke každému úkolu máme popsaný Ready for development, Definition of Done
* Klademe velký důraz na automatizaci, ač rozhodně nejsme v cílovém stavu
* Unit testy (JUnit 5)
* Integrační testy (Junit 4 v procesu migrace na 5)
* E2e testy ([https://www.cypress.io](https://www.cypress.io))
* Držíme meetingy
* Standup (každé ráno 15minut)
* Grooming - seznámení se a odhadování stories ve story pointech (míře komplexity), 1 hodina týdně
* Capacity planning, před koncem sprintu, plánujeme kapacitu na další sprint
* Sprint commitment & start (před polednem 1. den týdne)
* Retrospektivy, 1x měsíčně
* Kaizen, co nás trápí, co a jak můžeme zlepšit

Protože pracujeme distribuovaně - Zlín, Brno, Praha, Plzeň, všechny meetingy jsou online.

## Google workspace {#google-workspace}

Interně používáme Google Workspace stack pro vnitrofiremní komunikaci.

* Gmail
* Kalendář
* Google Meet na video chat/meetingy
* Google Drive
* Docs
* Sheets
* Pro občasnou komunikaci a synchronizaci s externími firmami používáme Slack. Prošli jsme si i vlastní šelf-hostovanou instanci Rocket.Chat, kterou jsme opustili, nechtěli jsme se zabývat její správou a provozem.


## Asana

Umožňuje nám spravovat projekty, firemní procesy i dílčí úkoly a zefektivňovat tak naši spolupráci, specielně v produktovém oddělení. Díky tomu máme přehled, řídíme práci snadněji, s menším množstvím chyb a s kvalitnějším výstupem. 

## DEV tools {#dev-tools}

IntellijIDEA

* Nejspíše netřeba představovat, momentálně asi nejoblíbenější IDE v Java světě

Visual Studio Code

* Lightweight
* Remote development (SSH)
* Angular & Java, univerzální editor
* Datagrip / DBeaver
  * Opět jebrains stack
* Jsme schopni sdílet a verzovat ad-hoc SQL kód
  
Kubernets tools

* Více v Kubernetes a Skaffold

Další užitečné informace

* [http://sqlfiddle.com/](http://sqlfiddle.com/)
* Pro izolované testování a ladění části DB, které jsme schopni snadno interně i externě nasdilet

## JIRA {#jira}

Používáme několik projektů v Jire, pro různá oddělení

* Business pro produkt
* Scrum pro vývoj
* Kanban pro IT/DevOps

Pluginy, bez kterých si nedovedeme představit v Jire fungovat

* [Worklogs](https://marketplace.atlassian.com/apps/1216516/worklogs-report?hosting=cloud&tab=overview)
  * 1.2 USD/uživatel/měsíc
  * Umožňuje nám vykazovat odvedenou práci
* [Issue checklist Free](https://marketplace.atlassian.com/apps/1220209/issue-checklist-for-jira-free?hosting=cloud&tab=overview)
  * Zdarma
  * Checklisty k úkolům
  * Obzvlášť užitečné u repetetivních úkolů, kdy je potřeba v rámci úkolů projit seznamem jednotlivých kroků
* [Structure](https://marketplace.atlassian.com/apps/34717/structure-project-management-ať-scale?hosting=cloud&tab=overview)
  * 1 USD/uživatel/měsíc
  * Umožňuje zobrazit reporty v uživatelsky definované formě
  * S neomezeným zanořením - ideální pro přehled nad více projekty/epicy

## Gitlab {#gitlab}

Na hostování zdrojových kódů používáme Gitlab spolu s Gitlab-CI. Používáme téměř všechny její funkce, merge requesty na review počínaje, automatizaci deploymentu konce. Vyčítáme a evidujeme v tomto prostředí běhy testu.

Používáme Git-flow

![Branching Git flow](git-flow.png)

## Branche
  * master - obraz produkce
  * develop - změny pro další release (kdykoliv deployovatelná)
  * feature/xx - z develop branche, úkoly, na kterých se pracuje - máme provázáno s Jira, tzn. z branche se lze proklikout do Jira úkolu, a Jira k úkolu naváže merge requesty (branche v gitlabu)
  * hoftix/xx - hotfixy, z master branche
  * Merge request zakládá a review dělají 1-2 vývojáři, v závislosti na složitosti / potenciálnímu dopadu na vyvíjenou funkcionalitu
  * Automatizace nepustí špatný build zmergovat (compile/unit test/integrační test/e2e test)

## Zdroje:

https://nvie.com/posts/a-successful-git-branching-model/ 

{{< jobs >}}
