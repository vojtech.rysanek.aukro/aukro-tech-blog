---
title: Retrospektiva roku 2021
date: 2022-01-26T23:00:00Z
publishdate: 2022-01-26T23:00:00Z
tags:
- retro
comments: false
hide_cover_on_detail: true

---
### Krátký úvod CTO

Při mém příchodu do Aukra byla celá platforma provozována a udržována pouze několika interními vývojáři, přičemž ze zkušenosti vím, že podobně velké e-commerce platformy mají násobně velká IT oddělení. Bylo potřeba stabilizovat systém, IT team a v něm běžící procesy, postupně do něj přijmout další kolegy a s nově vzniklou strukturou týmů začít řešit nové požadavky, i starší problémy, systém krok za krokem vylepšovat. Přestože není hotovo, již nyní těm spolupracovníkům, co do procesu vstoupili do této doby i s námi vydrželi, tleskám a děkuji, byla to výzva. Pro hrstku lidí, které Aukro v IT oddělení mělo k dispozici, **bylo extrémně důležité dobře vybírat jak businessové cíle, na kterých budeme pracovat, tak ty technické**.

Ale vraťme se k vlastnímu systému. Celá platforma je navržena v enterprise levelu - stylem bankovních systémů - používáme mnoho moderních, netriviálních technologií, od frontendu v Angularu (PWA na mobilních zařízeních), s server-side renderingem - Angular Universal, až po backendy obsluhovaných primárně Java službami, postavených na Spring/Boot, Quartz, Redis, Elastic Search, PostgreSQL, RabbitMQ a [dalších](https://tech.aukro.cz/blog/stack-we-use/).

Pro představu informace v číslech z konce roku 2021:

* DB obsahuje 100 miliónů inzerátů, kapacitně cca 1.4 TB
* 4 miliony příhozů měsíčně
* Elastic Search - 200 GB, 8 shard
* téměř 15 TB obrázků

### Cíle

Vytyčili jsme si několik cílů. Některé větší, náročnější, některé jednodušší, menší.

1. **Migrace platformy** - tzn. celou platformu ze stávající, ansiblem zpola-automatizované, na virtuálních serverech provozované, a manuálně firewall propojené, zmigrovat do něčeho moderního a “aktuálního”.   
   Kubernetes byla jasná volba - umožňuje nám teď celou platformu provozovat v produkci a na mnoha dalších vývojových prostředích - [Vývoj v Aukru | Tech Blog - Aukro.cz](https://tech.aukro.cz/blog/our-setup/). Celou platformu bylo tedy nutné zdockerizovat a připravit Kubernetes clustery na našem vlastním HW, setup clusterů jsme se rozhodli formou dodávky od externího dodavatele.
2. **Stabilizace platformy**, rychlosti a vylepšení monitoringu platformy k získání dat a informací pro posouzení funkčnosti platformy, běhu procesů v ní. Na základě získaných informací bylo možné lépe vidět úzká hrdla systému, tzn. bylo jasnější, kam a jak následně musíme zacílit kapacity vývoje.

   Drtivá většina závislosti a serverů byla několik let bez aktualizací - Elastic Search ve verzi 2.x, Spring MVC na platformním projektu, mnoho mvn balíčků léta bez aktualizací.
3. **Menší, přesto významné cíle** - např. vyčištění stovek mrtvých branchi, které v Gitlabu předchozí dodavatelé zanechali, kvanta mrtvého kódu napříč celou platformou, opuštěné MVP projekty atd. Upgrade z Java 8 na 11, který jsme v čase změnili na 17 (díky déle trvající migraci na nejnovější Spring Boot upgrade Javy dořešíme až na začátku roku 2022. V plánu byl i nový UI kit, časově jsme zatím nedokončili).
4. Další, neméně významnou výzvou, bylo **zrevidovat existující procesy** v IT a posunout je na vyšší level.

   Ve směru procesních změn jsme si dobře rozumněli s jednou z našich prvních IT posil [Janem Hrbotickým](https://www.linkedin.com/in/jan-hrboticky/) a začli jsme vše posouvat dále.

### Druhá polovina roku 2021

V polovině roku 2021 se k našemu teamu přidal **ostřílený, businessově orientovaný, [Jan Sadílek](https://www.linkedin.com/in/sadilekjan/)**, který nastavil nový, rozvojově-expanzní směr. Spolu s našimi vlastníky PFC a Leverage rozpohyboval mnohé nápady a větší projekty, které kvůli nedostatečným kapacitám pouze seděly na poličce. Spolu s vytyčenými businessovými horizonty se otevřel prostor pro výraznější hiring do IT. Během několika krátkých měsíců se IT Aukra rozrostlo na čtyřnásobek, a tyto projekty se začaly hýbat, např. pohyblivý konec aukce, vylepšení stávajících sekcí a přidání dalších (aukro-auta, aukro-reality), připravují se i kolekce aukcí a další. Business focus, orientace na čísla, to byl nový vítr, který jsme potřebovali.

Platforma však stále nebyla úplně zdravá a spolu s business focusem bylo třeba vyvážit řešení mnoha technických dluhů, které, jak to tak často bývá, mají tendenci se projevit ve chvíli, kdy to je nejméně potřeba - Vánoce 2021 a pád diskového pole.

Spolu s uvedenými kroky a výsledky jsme od poloviny roku v IT oddělní narostlí na téměř 30 kolegů, celkově se Aukro rozrostlo na téměř současných 80 aukráků. Z 1 teamu se vývoj rozšířil a rozdělili po doménách na teamy 4, a koncem roku se k nám přidala i full-time Scrum masterka. To nám umožnilo se věnovat paralelně více nejen business projektům, ale i samotnému technickému posunu platformy.

Přes všechny ups and downs si osobně myslím, že IT Aukra v roce 2021 se posunulo výrazně kupředu. Samozřejmě, něco se podařilo více, něco méně či vůbec, bod po bodu si rozebereme dále.

**Co se nám v roce 2021 podařilo:**

* **​​Přepracovali jsme platový & bonus systém**
  * Bonusy přidělujeme každý měsíc na základě ownershipu - vlastnění úkolů od A do Z, mimopracovní aktivita a zájem o posun sama sebe nebo platformy kupředu
* **SCRUM** - zavedli jsme SCRUM, de facto standard agilního vývoje
  * Tradiční meetingy, které teď pravidelně držíme, jako standup, planning, retrospektiva i review
* **Hiring**
  * Z původních cca 6 vývojářů a jednoho teamu, kteří celou platformu Aukra udržovali, jsme se rozrostli na téměř 30 vývojářů a dále IT team rozšiřujeme o nové pracovníky
  * Z ryze Zlínského vývojového centra jsme se rozrostli napříč celou ČR i Slovenskem - kanceláře má firma nejen ve zmíněném Zlíně, ale i v Brně a Praze
  * Došlo k posílení interního HR - [Giang Nguyen Thu](https://www.linkedin.com/in/giang-nguyen-thu-b16474b8/)
  * Stále hledáme šikovné lidi - Angular, Java, QA automatizace - neváhejte se ozvat na [kariera@aukro.cz](mailto:kariera@aukro.cz) nebo na [LinkedIn](https://www.linkedin.com/in/vojtechrysanek/)
* **Unit testy & E2e testy**
  * Z prakticky 0 počtu unit testů jsme se dostali téměř k 1k, brzy začneme zavádět code coverage jako podmínku úspěšného buildu
  * Spuštění e2e testu jako součást pipeliny na serverů, předtím bylo možné pouze manuálně z PC testera
* **Elastic Search**
  * Upgrade z verze 2.x na nejnovější verzi, úprava všech queries
* **Monitoring**
  * Grafana
    * Ucelená vizualizace provozu, předtím pouze pár dílčích grafů, hlavním zdrojem dat byl Zabbix
  * Kibana & APM
    * Upgrade na nejnovější verzi
    * Zavedení APM - monitoring toho, co se na platformě na pozadí děje, předtím pouze logy
* Přechod z Jenkins na Gitlab CI
  * Vytvoření nových Pipeline
  * Propojení s Kubernetes clusterem
* **Zrychlení vývoje** - setup lokálního PC
  * Díky Kubernetes
    * Nový vývojář může začít vyvíjet již první den v práci (předtím vyžadovalo cca 1 týden nastavování a setupovani prostředí pro vývoj)
  * Možno vyvíjet proti clusteru nebo lokálnímu Minikube
* **Quarkus a Vert.X**
  * Na některé menší microservicy jsme použili moderněji-reaktivně orientované frameworky, líbí se nám a chceme je využívat v budoucnu na další, [Vert.X vs Quarkus](https://tech.aukro.cz/blog/vert-x-quarkus/)
* **Canary deployment**
  * Předtím se releasovalo tak, že část platformy se ručně odpojila (), nyní vše automatizované - musí být backward kompatibilita
  * Musí se držet zpětná kompatibilita, pak by neměl být žádný výpadek
    * Doladěni zpětné kompatibility frontendu chceme posunout Q1 2022
* Zavedení CDN s obrázky
  * denně máme provoz obrázku cca 1TB
  * statické assety (js/woff/css) chystáme na Q1 2022
* Security audit
  * Po migraci do Kubernetes jsme si nechali jsme si vypracovat bezpečnostní audit portálu
  * Máme nastavenou bounty
* **Performance testy** 

**Co mohlo být lepší:**

* **Přechod 1 - 4 týmy**, hiring na 4X původních IT kapacit
  * Výběr vhodného SCRUM mastera nám trval, 1 SCRUM team byl již zbytečně velký a s tím se komplikovala i jeho organizace
    * procesně jsme již na to byli připraveni - počátkem roku by takto hirovat prakticky nebylo možné
  * Přestože jsme primárně hirovali medior-senior vývojáře, tak i oni byli z pohledu samotného Aukra “nováčci”, kteří se do ekosystému, služeb, a obecně znalostí Aukra museli zprvu zapracovat, a jak to tak bývá, vývoj po tu omezenou dobu zbrzdili
    * To ovlivnilo dodání jak business tak technických projektů
* **Kubernetes migrace**
  * Znalost Dockeru počátkem roku 2021 byla v Aukru téměř nulová
    * Celou doménu prakticky ownoval pouze [Filip Kulíšek](https://www.linkedin.com/in/filip-kul%C3%AD%C5%A1ek-1b30a4184/) - kudos
  * Externí partner, najmutý dříve naším IT dodavatelem, ve skutečnosti více sliboval než dělal, a přípravné práce **kubernetes migrace** se nám tak opozdily zhruba o 3 měsíce proti původnímu plánu
    * Na jeho neschopnost dodávat v čase a požadované kvalitě jsme měli reagovat o 2 měsíce dříve
  * Připravenost služeb na benefity a provoz na platformě Kubernetes
    * Služby, které startují do připraveného stavu obsluhovat požadavky zákazníků několik minut nejsou pro Kubernetes vhodné, de facto většinu zajímavějších funkcí z tohoto důvodu zatím nevyužíváme, o to více se těšíme na rok 2022, kdy se na technickém poli posuneme dále - jak v samotných službách a jejich separaci, tak v jejich orchestraci
* **Stabilita platformy**
  * I přes úsilí, které jsme stabilizaci platformy věnovali, je stále co zlepšovat, zdaleka nejsme v cílovém stavu, jakýkoliv výpadek nám přidělává vrásky na čele:
    * Na Vánoce 2021 došlo k pádu diskového pole, a tak v rozmezí cca 3 týdnů kolem Vánoc jsme se my, a bohužel i naší zákazníci, museli potýkat s výpadky obrázků a potažmo pak krátkodobě i celé platformy, museli jsme diskové pole rozšířit, zakoupit mezi svátky nové a za běhu celé platformy traffic mezi celkem tři diskové pole rozložit, dostupnost našich služeb:
      * platforma jako celek byla v prosinci dostupná z 99.296 %, tedy za celý prosinec jsme 5 h 8 m nebyli schopni obsloužit požadavky našich zákazníků
      * diskové pole obrázku mělo dostupnost pouze 97.270 %, což už je 19 h 56 m výpadku - mrzí nás to, protože po tuto dobu necelých 20 hodin nebyly některé obrázky nabídek vidět, a nové obrázky nebylo možné nahrávat
    * Každý výpadek/nedostupnost nás vůči zákazníkům mrzí a snažíme se dělat maximum pro to, aby se neopakoval, stabilní provoz platformy je naší vizitkou a uvědomujeme si, že konec roku v tomto směru nebyl optimální.
  * **Automatizace testů** je stále na nízké, nevyhovující úrovni
    * Navýšené unit testy (coverage) stále nejsou na dostatečné úrovni
    * e2e testy pokrývají pouze nejpodstatnější scénáře
    * Postrádané integrační testy nám zásadně chybí
      * máme ambici v roce 2022 toto výrazně posunout
* **Business vs. Tech**
  * Business, nové funkce a trhy nás živí, **klíčové je najít správný balance mezi tím, co nového do platformy přidáme a tím, jak samotná platforma běží**, ignorovat špatný stav platformy, ale stavět na ní další funkce, to se dlouhodobě nevyplácí

**Nepodařilo se dosud & plánujeme v průběhu roku 2022:**

* Automatizace release procesu - řešíme průběžně
  * SonarQube & Code coverage
    * Statická analýza kódu, měření pokrytí testy
  * Automatizace - testy
    * Chybějící integrační testy + málo e2e use času k tomu, aby se dalo výrazně rychleji releasovat
  * Přechod přes automatizaci do continuous delivery módu
* **Spring Boot nejnovější verze - Q1 2022**
  * Nedotažen včas, zdržuje nás v mnoha dalších technických zlepšeních
    * Metriky
    * Rozpad do microservices
  * Integrační testy, TestContainers
* **Rozpad do více Microservices**
  * Oddělení analytických, historizačních služeb od platformy, včetně separátní databáze
  * Dotažení Kubernetes ready služeb
    * Kubernetes počítá s tím, že služba nastartuje ideálně v rámci sekund, my jsme schopni nastartovat službu spíše za více než minutu, a to plánujeme brzy změnit
      * Bidding service - Quarkus
      * Query service - Quarkus
      * Analytické, historizační služby, a další
* **Upgrade PostgreSQL - Q1-2022**
  * Na stávající verzi 9.6 chybí mnoho funkcí, některé změny vyžadují výpadkové releasy - odstávky platformy
* **Očištění a odladění platformy**
  * Chyby - platforma obsahuje stále velké množství chyb, o kterých víme, ale díky výraznému business focusu nejsou prozatím významnou prioritou, nejsou-li zcela zásadní a nijak neovlivňují běh celého systému

**Naše nejpodstatnější technologické výzvy a cíle roku 2022:**

* Připravit platformu na expanzi do zahraničí
  * SK
  * a další země v našem regionu
* Continuous delivery - Automatizace
* Stabilita platformy - Automatizace, Integrační testy
* Chyby - pravidelné a průběžné zpracovávání chyb, o kterých víme a trápí nejen nás i naše zákazníky

{{< jobs >}}
