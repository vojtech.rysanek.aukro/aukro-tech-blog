---
title: "Vyvíjíme v cloudu"
date: 2020-10-01T11:40:11+02:00
publishdate: 2020-10-01T11:40:11+02:00
tags: ["process", "cloud"]
comments: false
---

Vývojářům používajícím Visual Studio Code s pluginem pro remote development umožňujeme automatizovat zřízení unifikovaného vývojového prostředí a tímto způsobem jim možno vyvíjet prakticky odkudkoliv, v extrému i na chromebooku či podobném zařízení, na kterém je Visual Studio Code možno spustit.

Důvody

* Výkon - prostředí, ve kterém vývojář vyvíjí je možno nasizovat tak, jak je v danou chvíli potřeba, tzn. např. jdeme-li re-trénovat machine learning model, přidáme GPU, paměť, a po re-trénování můžeme snížit náklady a efektivně je držet pod kontrolou
* Nezávislost na prostředí/HW
  * Windows
  * Mac
* Bezpečnost
  * Správa a údržba HW, ramsoware
  * Automatické zálohování
* Uložením zdrojových kódů je na serveru, dosahujeme  mnohem vyšší bezpečnosti 
* Nestárne

Kde umíme takové prostředí provozovat

* Vlastní infrastruktura
* AWS / Google cloud
* Hetzner

## Setup {#setup}

Pre-setup image

* Máme předem vytvořeny a uloženy image, se všemi potřebnými závislosti pro vývoj
* Docker
* JVM
* Kubectl
* VPN do interní sítě

Attach disk

* SSD, 500 GB
{{< highlight bash "linenos=table,hl_lines=8 15-17,linenostart=199" >}}
DEVICENAME=<device name>
sudo mkdir -p /mnt/home
sudo mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard /dev/${DEVICENAME}
sudo mount -o discard,defaults /dev/${DEVICENAME} /mnt/home
sudo rsync -avHAX /home/ /mnt/home
sudo umount /mnt/home
sudo mount /dev/${DEVICENAME} /home
UUID="$(sudo blkid -s UUID -o value /dev/sdb)"
echo "UUID=${UUID} /home ext4 discard,defaults,nofail 0 2" | sudo tee -a /etc/fstab > /dev/null
{{< / highlight >}}

Platí, a je třeba na to nezapomínat, že cokoliv nainstalujete/provedete mimo /mnt/home může být ztraceno při upgrade na novější image.

User setup

* git config --globál user.email vojtech.rysanek@aukro.cz
* git config --globál user.name "Vojtěch Ryšánek"

Výhodou tohoto setupu je jeho dynamičnost - možno přidat gitlab.či runner na svoje projekty (re-tranovani ML modelu)

## Visual Studio Code integrace {#Visual-studio-code-integrace}

Seamless přes [https://code.visualstudio.com/docs/remote/ssh](https://code.visualstudio.com/docs/remote/ssh) plugin.

* tunelování jakéhokoliv portu na lokální
* aplikace se tváří, jakoby běžela lokálně, podobně jako [Skaffold]({{< ref "/blog/kubernetes-skaffold#skaffold" >}} "Skaffold")

### Gitpod - Cloud IDE {#gitpod-cloud-ide}

Weova verze Visual studio code, která je přímo Gitlabe & dalšími podporovna

[https://www.gitpod.io/](https://www.gitpod.io/)

* Umožňuje okamžitě pracovat na kódu s předehřátými prostředími (branchemi)
* Netřeba čekat na stažení balíčku & závislosti, stačí přijít a pracovat
* Možnost psát kód na levném notebooku/ipadu odkudkoliv s internetem
* Možno instalovat na kubernetes-cluster
* Případně lze hostovat na AWS/GCP

## VPN/tunneling {#vpn-tunneling}

Pro případ, že chceme interně vzájemné přistupovat na služby spuštěné buď v lokálním clusteru nebo PC, využíváme [https://www.zerotier.com/](https://www.zerotier.com/) nebo [https://inlets.dev](https://inlets.dev)

U [Skaffoldu]({{< ref "/blog/kubernetes-skaffold#skaffold" >}} "Skaffold") je situace o něco snazší, tam lze většinu věcí rovnou routovat na interní / veřejnou adresu dle potřeby, a která je tak pro testera/product ownera uvnitř přístupná.

## Kde se můžete dočíst víc {#kde-se-můžete-dočíst-víc}

[https://borderux.com/remote-development-is-the-future/](https://borderux.com/remote-development-is-the-future/)

[https://blog.cloudflare.com/how-replicated-secured-our-remote-dev-environment-with-cloudflare-access/](https://blog.cloudflare.com/how-replicated-secured-our-remote-dev-environment-with-cloudflare-access/)

[https://engineerworkshop.com/blog/how-to-set-up-a-remote-development-server/](https://engineerworkshop.com/blog/how-to-set-up-a-remote-development-server/)

[https://stripe.com/blog/remote-hub](https://stripe.com/blog/remote-hub)

[https://stripe.com/en-cz/atlas/guides/scaling-eng](https://stripe.com/en-cz/atlas/guides/scaling-eng)

[https://cloud.google.com/blog/products/application-development/build-a-dev-workflow-with-cloud-code-on-a-pixelbook](https://cloud.google.com/blog/products/application-development/build-a-dev-workflow-with-cloud-code-on-a-pixelbook)

{{< jobs >}}