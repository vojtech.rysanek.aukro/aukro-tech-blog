# Demo

This is the demonstration site of the [Mediumish GoHugo Theme](https://github.com/lgaida/mediumish-gohugo-theme)
Run `hugo server -w -v`

Docker run - `docker run --rm -it -v $(pwd):/src -p 1313:1313  klakegg/hugo:0.80.0-ext-ubuntu server -D`

